# 根据id+listId+平台ID+站点查询一条审核信息

> URL: http://127.0.0.1:8074/controller-forecastCheckService/front/getForecastCheckInfo/71/364/101/DE
>
> Origin Url: http://127.0.0.1:8074/controller-forecastCheckService/front/getForecastCheckInfo/{id}/{listId}/{platformId}/{site}
>
> Type: GET


### Request headers

|Header Name| Header Value|
|---------|------|
|Authorization|bearer 9f184668-a0f6-4f6a-a59d-c7c044a4c892|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|
|id|Number|1||
|listId|Number|1||
|platformId|Number|1||
|site|String|site_639s7||


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Body parameters

###### JSON

```

```

###### JSON document

```

```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```
{
  "code": 200,
  "message": "OK",
  "data": {
    "id": 71,
    "sku": "00118061",
    "optimalSonSku": "ZCJRQYTJ8KWDHXNYJV0",
    "site": "DE",
    "skuName": "驻车加热器一体机12v 8kw单风口 黑旋钮液晶款",
    "groupName": "Auto-Repair2",
    "stockId": 4,
    "stockName": "欧洲市场",
    "forecastListId": 364,
    "platformId": 101,
    "platformModel": "VC",
    "platformName": "amazon",
    "reviewStatus": 1,
    "forecastReason": "节⽇促销",
    "averageWeeklyTotalBefore": 0.0000,
    "averageWeeklyTotalAfter": 0.0000,
    "createdTime": "2022-11-30 14:20:47"
  }
}
```

##### Response document
```
{
	"code":"No comment,Type =Number",
	"message":"No comment,Type =String",
	"data":{
		"id":"主键id",
		"adjustId":"调整id",
		"adjustContent":"调整内容",
		"sku":"sku",
		"optimalSonSku":"最优子SKU",
		"site":"站点",
		"skuName":"产品名称",
		"groupName":"所属小组",
		"stockId":"市场对应的Id",
		"stockName":"市场对应的名称",
		"forecastListId":"预估列表id(forecast_list)",
		"platformId":"平台Id",
		"platformModel":"平台模式",
		"platformName":"平台名称",
		"reviewStatus":"审核状态 1.已审核,0.待审核,2.审核不通过",
		"submitPeople":"提交人",
		"forecastReasonId":"修改备注",
		"forecastReason":"修改备注对应的文字内容",
		"averageWeeklyTotalBefore":"SKU不同平台的修改前覆盖周平均",
		"averageWeeklyTotalAfter":"SKU不同平台的修改后覆盖周平均",
		"createdTime":"创建时间",
		"createdBy":"创建人",
		"updatedTime":"更新时间",
		"putawayTime":"第一次上架时间",
		"updatedBy":"更新人",
		"isDelete":"是否被删除，0：未删除 1：已删除"
	},
	"page":{
		"total":"No comment,Type =Number",
		"totalPage":"No comment,Type =Number",
		"items":[
			{}
		],
		"currentPage":"No comment,Type =Number",
		"pageSize":"No comment,Type =Number"
	}
}
```


