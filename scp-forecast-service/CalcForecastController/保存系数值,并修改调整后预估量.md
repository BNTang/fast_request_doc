# 保存系数值,并修改调整后预估量

> URL: http://127.0.0.1:8074/controller-calcForecastService/front/saveAdjustNum
>
> Origin Url: http://127.0.0.1:8074/controller-calcForecastService/front/saveAdjustNum
>
> Type: POST


### Request headers

|Header Name| Header Value|
|---------|------|
|Authorization|bearer 9f184668-a0f6-4f6a-a59d-c7c044a4c892|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Body parameters

###### JSON

```
[
  {
    "id": 1,
    "forecastListId": 1,
    "weekNum": 1,
    "tpWeek": 1,
    "biWeek": 1,
    "saleNum": 1,
    "negativeProfitSaleNum": 1,
    "adjustForecast": 1,
    "isRedWeek": true,
    "noStockDay": 1,
    "forecastListDetailList": [
      {
        "id": 1,
        "platformId": 1,
        "site": "site_25fnu",
        "saleWeekPlatform": 1,
        "negativeProfitSaleWeek": 1,
        "negativeSellOut": 1,
        "saleBiPlatform": 1,
        "forecastPlatform": 1,
        "adjustNum": 1,
        "adjustForecast": 1,
        "isCalculationWeek": true,
        "isDisabled": true,
        "isRedWeek": true,
        "noStockDay": 1
      }
    ],
    "isCurrentWeek": true
  }
]
```

###### JSON document

```
[
  {
    "id": "No comment,Value =1",
    "forecastListId": "列表id",
    "weekNum": "周数",
    "tpWeek": "周销售额（TP）",
    "biWeek": "周BI",
    "saleNum": "周销量(总)不含负利润销量",
    "negativeProfitSaleNum": "周销量(总)含负利润销量",
    "adjustForecast": "调整后预估量(总)",
    "isRedWeek": "是否为断货周 0代表假1代表真",
    "noStockDay": "断货天数",
    "forecastListDetailList": [
      {
        "id": "No comment,Value =1",
        "platformId": "平台",
        "site": "站点",
        "saleWeekPlatform": "平台周销量（不含负利润的销量）",
        "negativeProfitSaleWeek": "负利润周销量（含负利润的销量）",
        "negativeSellOut": "sell-out 的负值",
        "saleBiPlatform": "平台sale/bi",
        "forecastPlatform": "平台预估量",
        "adjustNum": "X",
        "adjustForecast": "调整预估量",
        "isCalculationWeek": "是否为参与计算的周 0代表假1代表真",
        "isDisabled": "是否置灰 1是 0否",
        "isRedWeek": "是否为断货周 0代表假1代表真",
        "noStockDay": "断货天数"
      }
    ],
    "isCurrentWeek": "是否是当前周（标绿）"
  }
]
```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```
{
  "code": 9001,
  "message": "\r\n### Error updating database.  Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :'     WHERE id IN', expect SET, actual WHERE pos 52, line 1, column 28, token WHERE : UPDATE forecast_list_detail\n          \n        WHERE id IN\r\n### The error may exist in com/vevor/scp/forecast/mapper/forecast/ForecastListDetailMapper.xml\r\n### The error may involve com.vevor.scp.forecast.mapper.forecast.ForecastListDetailMapper.batchUpdateListDetailById\r\n### The error occurred while executing an update\r\n### SQL: UPDATE forecast_list_detail                    WHERE id IN\r\n### Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :'     WHERE id IN', expect SET, actual WHERE pos 52, line 1, column 28, token WHERE : UPDATE forecast_list_detail\n          \n        WHERE id IN\n; uncategorized SQLException; SQL state [null]; error code [0]; sql injection violation, syntax error: syntax error, error in :'     WHERE id IN', expect SET, actual WHERE pos 52, line 1, column 28, token WHERE : UPDATE forecast_list_detail\n          \n        WHERE id IN; nested exception is java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :'     WHERE id IN', expect SET, actual WHERE pos 52, line 1, column 28, token WHERE : UPDATE forecast_list_detail\n          \n        WHERE id IN"
}
```

##### Response document
```
{
	"code":"No comment,Type =Number",
	"message":"No comment,Type =String",
	"data":{},
	"page":{
		"total":"No comment,Type =Number",
		"totalPage":"No comment,Type =Number",
		"items":[
			{}
		],
		"currentPage":"No comment,Type =Number",
		"pageSize":"No comment,Type =Number"
	}
}
```


