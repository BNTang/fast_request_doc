# 保存修改的x

> URL: http://127.0.0.1:8074/controller-calcForecastService/front/v2/saveChangeX
>
> Origin Url: http://127.0.0.1:8074/controller-calcForecastService/front/v2/saveChangeX
>
> Type: POST


### Request headers

|Header Name| Header Value|
|---------|------|
|Authorization|bearer 9f184668-a0f6-4f6a-a59d-c7c044a4c892|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Body parameters

###### JSON

```
{
  "listId": "324",
  "platformId": 101,
  "site": "US",
  "adjustXList": [
    {
      "x": 1,
      "week": 1
    },
    {
      "x": 1,
      "week": 2
    },
    {
      "x": 1,
      "week": 3
    },
    {
      "x": 1,
      "week": 4
    },
    {
      "x": 1,
      "week": 5
    },
    {
      "x": 1,
      "week": 6
    },
    {
      "x": 1,
      "week": 7
    },
    {
      "x": 1,
      "week": 8
    },
    {
      "x": 1,
      "week": 9
    },
    {
      "x": 1,
      "week": 10
    },
    {
      "x": 1,
      "week": 11
    },
    {
      "x": 1,
      "week": 12
    },
    {
      "x": 1,
      "week": 13
    },
    {
      "x": 1,
      "week": 14
    },
    {
      "x": 1,
      "week": 15
    },
    {
      "x": 1,
      "week": 16
    },
    {
      "x": 1,
      "week": 17
    },
    {
      "x": 1,
      "week": 18
    },
    {
      "x": 1,
      "week": 19
    },
    {
      "x": 1,
      "week": 20
    },
    {
      "x": 1,
      "week": 21
    },
    {
      "x": 1,
      "week": 22
    },
    {
      "x": 1,
      "week": 23
    },
    {
      "x": 1,
      "week": 24
    },
    {
      "x": 1,
      "week": 25
    },
    {
      "x": 1,
      "week": 26
    },
    {
      "x": 1,
      "week": 27
    },
    {
      "x": 1,
      "week": 28
    },
    {
      "x": 1,
      "week": 29
    },
    {
      "x": 1,
      "week": 30
    },
    {
      "x": 1,
      "week": 31
    },
    {
      "x": 1,
      "week": 32
    },
    {
      "x": 1,
      "week": 33
    },
    {
      "x": 1,
      "week": 34
    },
    {
      "x": 1,
      "week": 35
    },
    {
      "x": 1,
      "week": 36
    },
    {
      "x": 1,
      "week": 37
    },
    {
      "x": 1,
      "week": 38
    },
    {
      "x": 1,
      "week": 39
    },
    {
      "x": 1,
      "week": 40
    },
    {
      "x": 1,
      "week": 41
    },
    {
      "x": 1,
      "week": 42
    },
    {
      "x": 1,
      "week": 43
    },
    {
      "x": 1,
      "week": 44
    },
    {
      "x": 1,
      "week": 45
    },
    {
      "x": 1,
      "week": 46
    },
    {
      "x": 1,
      "week": 47
    },
    {
      "x": 1,
      "week": 48
    },
    {
      "x": 1,
      "week": 49
    },
    {
      "x": 1,
      "week": 50
    },
    {
      "x": 1,
      "week": 51
    },
    {
      "x": 1,
      "week": 52
    }
  ],
  "reasonIds": [
    14
  ]
}
```

###### JSON document

```
{
  "listId": "listId",
  "platformId": "平台id",
  "site": "站点",
  "adjustXList": [
    {
      "week": "周",
      "x": "x值"
    }
  ],
  "remark": "备注",
  "reasonIds": [
    "修改原因ids"
  ]
}
```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```
{
  "code": 9001,
  "message": "X 值未改动，不能保存！"
}
```

##### Response document
```
{
	"code":"No comment,Type =Number",
	"message":"No comment,Type =String",
	"data":{
		"java.lang.String":"No comment,Type =String"
	},
	"page":{
		"total":"No comment,Type =Number",
		"totalPage":"No comment,Type =Number",
		"items":[
			{}
		],
		"currentPage":"No comment,Type =Number",
		"pageSize":"No comment,Type =Number"
	}
}
```


