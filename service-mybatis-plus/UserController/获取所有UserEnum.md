# 获取所有UserEnum

> URL: 127.0.0.1:40001/user/getUserEnumList
>
> Origin Url: 127.0.0.1:40001/user/getUserEnumList
>
> Type: GET


### Request headers

|Header Name| Header Value|
|---------|------|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Body parameters

###### JSON

```

```

###### JSON document

```

```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```
{"code":200,"data":[{"id":0,"name":"2","age":{"value":1,"desc":"一岁"},"email":"303158131@qq.com","grade":{"code":1,"desc":"小学"},"createTime":null,"updateTime":null},{"id":24,"name":"name","age":{"value":1,"desc":"一岁"},"email":"3031581312@qq.com","grade":{"code":3,"desc":"高中"},"createTime":null,"updateTime":null},{"id":1602913055184715778,"name":"BNTang","age":{"value":1,"desc":"一岁"},"email":"303158131@qq.com","grade":{"code":1,"desc":"小学"},"createTime":null,"updateTime":null},{"id":1608015441385406465,"name":"32","age":{"value":2,"desc":"二岁"},"email":"32","grade":{"code":2,"desc":"中学"},"createTime":null,"updateTime":"2022-12-29T12:50:19"},{"id":1608015551116787713,"name":"32","age":{"value":2,"desc":"二岁"},"email":"32","grade":{"code":2,"desc":"中学"},"createTime":null,"updateTime":null},{"id":1608016000599375874,"name":"32","age":{"value":2,"desc":"二岁"},"email":"32","grade":{"code":2,"desc":"中学"},"createTime":null,"updateTime":null},{"id":1608324631928700929,"name":"32","age":{"value":2,"desc":"二岁"},"email":"32","grade":{"code":2,"desc":"中学"},"createTime":"2022-12-29T12:50:48","updateTime":"2022-12-29T12:50:48"}],"message":"响应成功"}
```

##### Response document
```
{
	"code":"状态码",
	"message":"响应消息"
}
```


