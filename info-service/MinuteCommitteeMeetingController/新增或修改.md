# 新增或修改

> URL: 127.0.0.1:10006/minute-committee-meeting/submit
>
> Origin Url: 127.0.0.1:10006/minute-committee-meeting/submit
>
> Type: POST


### Request headers

|Header Name| Header Value|
|---------|------|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Body parameters

###### JSON

```
{
	"meetingTime":"2023-03-15",
	"organizationDept":1,
	"committeeId":1,
	"belongCommittee":1,
	"attaches":[
		{
			"name":"name_slgx4",
			"originalName":"originalName_xesn2",
			"type":"type_dq61r",
			"attachSize":1,
			"sourceType":1,
			"sourceId":1,
			"attachCode":"attachCode_fu49l",
			"uploaded":1,
			"minLearnTime":1,
			"companyCode":"companyCode_7g5uk",
			"createUserName":"createUserName_4aisu",
			"updateUserName":"updateUserName_kwxbc",
			"createDeptName":"createDeptName_2a8gh",
			"createCompanyName":"createCompanyName_toeo0",
			"createOfficeName":"createOfficeName_rbt7r",
			"id":1,
			"createUser":1,
			"createTime":"2023-03-15 17:39:39",
			"updateUser":1,
			"updateTime":"2023-03-15 17:39:39",
			"isDeleted":1,
			"createDept":1,
			"createOffice":1
		}
	],
	"companyCode":"companyCode_7l4ty",
	"createUserName":"createUserName_ayzvo",
	"updateUserName":"updateUserName_3y0fp",
	"createDeptName":"createDeptName_h6amu",
	"createCompanyName":"createCompanyName_fdz5p",
	"createOfficeName":"createOfficeName_g0bup",
	"id":1,
	"createUser":1,
	"createTime":"2023-03-15 17:39:39",
	"updateUser":1,
	"updateTime":"2023-03-15 17:39:39",
	"isDeleted":1,
	"createDept":1,
	"createOffice":1
}
```

###### JSON document

```
{
	"meetingTime":"会议时间",
	"organizationDept":"组织部门",
	"committeeId":"委员会id",
	"belongCommittee":"所属委员会 sms_dict: code: BELONG_COMMITTEE",
	"attaches":[
		{
			"name":"附件名称(文件服务器路径)",
			"originalName":"附件原名",
			"type":"文件后缀名",
			"attachSize":"附件大小",
			"sourceType":"来源类型",
			"sourceId":"来源id",
			"attachCode":"分组标识",
			"uploaded":"是否已经上传0:否 1:是",
			"minLearnTime":"课件学习最短学时（分钟）",
			"companyCode":"所属公司标识",
			"createUserName":"创建人姓名",
			"updateUserName":"更新人姓名",
			"createDeptName":"创建部门名称",
			"createCompanyName":"创建公司名称",
			"createOfficeName":"创建科室名称",
			"id":"主键id",
			"createUser":"创建人",
			"createTime":"创建时间",
			"updateUser":"更新人",
			"updateTime":"更新时间",
			"isDeleted":"是否已删除",
			"createDept":"创建部门",
			"createOffice":"创建科室"
		}
	],
	"companyCode":"所属公司标识",
	"createUserName":"创建人姓名",
	"updateUserName":"更新人姓名",
	"createDeptName":"创建部门名称",
	"createCompanyName":"创建公司名称",
	"createOfficeName":"创建科室名称",
	"id":"主键id",
	"createUser":"创建人",
	"createTime":"创建时间",
	"updateUser":"更新人",
	"updateTime":"更新时间",
	"isDeleted":"是否已删除",
	"createDept":"创建部门",
	"createOffice":"创建科室"
}
```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```
{"code":500,"success":false,"data":false,"msg":"服务器异常"}
```

##### Response document
```
{
	"code":"状态码",
	"success":"是否成功",
	"data":{
		"java.lang.Boolean":"No comment,Type =Boolean"
	},
	"msg":"返回消息"
}
```


