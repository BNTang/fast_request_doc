# 根据Id删除数据

> URL: 127.0.0.1:50001/deleteById/Dn08soQBNyDzYy-TKK1a
>
> Origin Url: 127.0.0.1:50001/deleteById/{id}
>
> Type: DELETE


### Request headers

|Header Name| Header Value|
|---------|------|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|
|id|String|Dn08soQBNyDzYy-TKK1a||


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Body parameters

###### JSON

```

```

###### JSON document

```

```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```
1
```

##### Response document
```
"No comment,Type =Number"
```


