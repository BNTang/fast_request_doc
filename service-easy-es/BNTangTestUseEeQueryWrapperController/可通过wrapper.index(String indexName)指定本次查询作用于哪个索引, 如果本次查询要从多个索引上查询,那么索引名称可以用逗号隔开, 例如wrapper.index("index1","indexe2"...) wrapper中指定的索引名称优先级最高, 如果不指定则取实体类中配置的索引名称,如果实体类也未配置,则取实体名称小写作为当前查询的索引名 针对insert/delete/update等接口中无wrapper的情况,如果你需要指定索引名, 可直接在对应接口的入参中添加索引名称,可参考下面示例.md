# 可通过wrapper.index(String indexName)指定本次查询作用于哪个索引, 如果本次查询要从多个索引上查询,那么索引名称可以用逗号隔开, 例如wrapper.index("index1","indexe2"...) wrapper中指定的索引名称优先级最高, 如果不指定则取实体类中配置的索引名称,如果实体类也未配置,则取实体名称小写作为当前查询的索引名 针对insert/delete/update等接口中无wrapper的情况,如果你需要指定索引名, 可直接在对应接口的入参中添加索引名称,可参考下面示例

> URL: 127.0.0.1:50001/eeQueryWrapper-controller/testIndex
>
> Origin Url: 127.0.0.1:50001/eeQueryWrapper-controller/testIndex
>
> Type: GET


### Request headers

|Header Name| Header Value|
|---------|------|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Body parameters

###### JSON

```

```

###### JSON document

```

```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```
{
  "code": 200,
  "data": {
    "id": "D33Hs4QBNyDzYy-TJq35",
    "title": "it6666",
    "content": "BNTang"
  },
  "message": "响应成功"
}
```

##### Response document
```
{
	"code":"状态码",
	"message":"响应消息"
}
```


